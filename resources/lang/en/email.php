<?php
return [
    'user_registered' => 'User registration success',
    'activation_mail_sent' => 'Activation mail sent',
    'reset_password_sent' => 'Reset password link sent successfully',
    'password_request' => 'کاربری درخواست تغییر رمز عبور شما را داده است.در صورتی که این درخواست متعلق به شما نیست این ایمیل را نادیده بگیرید.',
    'click_password_link' => 'برای تغییر رمز عبور روی لینک زیر کلیک کنید',
    'reset_password_link' => 'ریست رمز عبور',
    'send_reset_password_link' => 'ارسال لینک ریست رمز عبور',
    'user_not_found' => 'This user not exist',
];
