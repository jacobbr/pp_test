<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "رمز عبور باید حداقل شش کاراکتر و مطابقت داشته باشد.",
    "user" => "کاربری با این ایمیل آدرس یافت نشد.",
    "token" => "مشخصه ی بازنشاندن رمز عبور اشتباه است.",
    "sent" => "یاد آور رمز عبور ارسال شد.",
    "reset"    => "رمز عبور بازنشانی شد!",
    'invalid_link' => 'لینک نا معتبر است',
    'expired_link' => 'لینک منقضی شده است',
    'reset_error' => 'بروز خطا در تغییر رمز عبور',
    'success_reset' => 'رمز عبور شما با موفقیت تغییر کرد.',

];
