<?php
return [
    'create_file'     => 'فایل با موفقیت ایجاد شد',
    'create_directory'     => 'پوشه با موفقیت ایجاد شد',
    'error_in_create_directory' => 'خطا در ایجاد پوشه',
    'error_in_create_file' => 'خطا در ایجاد فایل',
];
