@extends('layout.layout')

@section('scripts')
    <script>
        $(document).ready(function() {
            getCaptcha();
            $('form').animate({
                height: "toggle",
                opacity: "toggle"
            }, 'slow');

            $('.message a').on('click', function() {
                $("#form-response").empty();
                $('form').animate({
                    height: "toggle",
                    opacity: "toggle"
                }, 'slow');
            });
        });
        function getCaptcha() {
            $.get('/api/v1.0/captcha', function(data, textStatus) {}).done(function(data) {
                $("#login-captcha-img").attr('src',data.img);
                localStorage.setItem('captcha_key', data.key);
            });
        }
        function login() {
            var loginEmailEntry = $("#login-email").val();
            var loginPasswordEntry = $("#login-password").val();
            var loginCaptchaEntry= $("#login-captcha").val();
            var createFormResponseObject = $("#form-response");
            $(createFormResponseObject).empty();
            var request = $.ajax({
                url: "/api/v1.0/authenticate",
                method: "POST",
                data: {
                    email: loginEmailEntry,
                    password: loginPasswordEntry,
                    captcha: loginCaptchaEntry,
                    captcha_key: localStorage.getItem('captcha_key')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                localStorage.setItem('user', response.user)
                localStorage.setItem('token', response.token);
                $(createFormResponseObject).removeClass("error");
                $(createFormResponseObject).addClass("success");
                let success = ["Successfully logged in"];
                success = success.map((name) => $("<li class='li-response'>").text(name));
                $(createFormResponseObject).append(success);
                setTimeout(function() {
                    $("#myModal").css('display', "block");
                }, 1);
                runningProcess();
            });
            request.fail(function (jqXHR) {
                $(createFormResponseObject).removeClass("success");
                $(createFormResponseObject).addClass("error");
                let errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li class='li-response'>").text(name));
                $(createFormResponseObject).append(errors);
            });
        }
        function register() {
            var createNameEntry = $("#create-name").val();
            var createPasswordEntry = $("#create-password").val();
            var createPasswordConfirmationEntry = $("#create-password-confirmation").val();
            var createEmailEntry = $("#create-email").val();
            var createFormResponseObject = $("#form-response");
            $(createFormResponseObject).empty();
            var request;
            request = $.ajax({
                url: "/api/v1.0/register",
                method: "POST",
                data: {
                    name: createNameEntry,
                    email: createEmailEntry,
                    password: createPasswordEntry,
                    password_confirmation: createPasswordConfirmationEntry
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $(createFormResponseObject).removeClass("error");
                $(createFormResponseObject).addClass("success");
                var success = ["Successfully create user"]
                success = success.map((name) => $("<li class='li-response'>").text(name));
                $(createFormResponseObject).append(success);
            });
            request.fail(function(jqXHR) {
                $(createFormResponseObject).removeClass("success");
                $(createFormResponseObject).addClass("error");
                var errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li class='li-response'>").text(name));
                $(createFormResponseObject).append(errors);
            });
        }
        function logout() {
            var request = $.ajax({
                url: "/api/v1.0/logout",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $("#myModal").css('display', 'none');
                localStorage.removeItem('token');
                localStorage.removeItem('user');
                document.location.href = '/';
            });
        }
        function runningProcess() {
            let request;
            request = $.ajax({
                url: "/api/v1.0/process",
                method: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $("#running-processes-table").empty()
                let processes = $.parseJSON(jqXHR.responseText).item;
                console.log(processes);
                let t = '<tr><th>User</th><th>PID</th> <th>%CPU</th> <th>%MEM</th> <th>VSZ</th> <th>RSS</th> <th>TTY</th> <th>STAT</th> <th>START</th> <th>TIME</th> <th>COMMAND</th> </tr>';
                $.each(processes, function(i, p) {
                    t += '<tr><td>'+p.USER+'</td><td>'+p.PID+'</td><td>'+p["%CPU"]+'</td><td>'+p["%MEM"]+'</td><td>'+p["VSZ"]+'</td><td>'+p["RSS"]+'</td><td>'+p["TTY"]+'</td><td>'+p["STAT"]+'</td><td>'+p["START"]+'</td><td>'+p["TIME"]+'</td><td>'+p["COMMAND"]+'</td></tr>';
                });
                console.log(t);
                $("#running-processes-table").append(t);
            });

            let i;
            const x = document.getElementsByClassName("tab");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById("running-processes").style.display = "block";
        }
        function files() {
            let request;
            request = $.ajax({
                url: "/api/v1.0/file/file",
                method: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $("#files-table").empty()
                let processes = $.parseJSON(jqXHR.responseText).item;
                console.log(processes);
                let t = '<tr><th>#</th><th>Filename</th></tr>';
                $.each(processes, function(i, p) {
                    t += '<tr><td>'+(i+1)+'</td><td>'+p+'</td></tr>';
                });
                $("#files-table").append(t);
            });

            let i;
            const x = document.getElementsByClassName("tab");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById("files").style.display = "block";
        }
        function directories() {
            let request;
            request = $.ajax({
                url: "/api/v1.0/file/directory",
                method: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $("#directories-table").empty()
                let processes = $.parseJSON(jqXHR.responseText).item;
                console.log(processes);
                let t = '<tr><th>#</th><th>Filename</th></tr>';
                $.each(processes, function(i, p) {
                    t += '<tr><td>'+(i+1)+'</td><td>'+p+'</td></tr>';
                });
                console.log(t);
                $("#directories-table").append(t);
            });
            let i;
            const x = document.getElementsByClassName("tab");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById("directories").style.display = "block";
        }
        function createFile() {
            let createFilenameObject = $("#create-file-name");
            let createFileResponseObject = $("#create-file-response");
            $(createFileResponseObject).empty();

            let request;
            request = $.ajax({
                url: "/api/v1.0/file/file",
                method: "POST",
                data: {
                    name: createFilenameObject.val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $(createFileResponseObject).removeClass("error");
                $(createFileResponseObject).addClass("success");
                let success = ["File create successfully"];
                success = success.map((name) => $("<li>").text(name));
                $(createFileResponseObject).append(success);
                setTimeout(function() {
                    createFileResponseObject.empty();
                }, 2000);
                files();
            });
            request.fail(function (jqXHR) {
                $(createFileResponseObject).removeClass("success");
                $(createFileResponseObject).addClass("error");
                let errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li>").text(name));
                $(createFileResponseObject).append(errors);
                setTimeout(function() {
                    createFileResponseObject.empty();
                }, 2000);
            });



        }
        function createDirectory() {
            let createDirectoryNameObject = $("#create-directory-name");
            let createDirectoryResponseObject = $("#create-directory-response");
            $(createDirectoryResponseObject).empty();

            let request;
            request = $.ajax({
                url: "/api/v1.0/file/directory",
                method: "POST",
                data: {
                    name: createDirectoryNameObject.val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'bearer' + localStorage.getItem('token')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $(createDirectoryResponseObject).removeClass("error");
                $(createDirectoryResponseObject).addClass("success");
                let success = ["File create successfully"];
                success = success.map((name) => $("<li>").text(name));
                $(createDirectoryResponseObject).append(success);
                setTimeout(function() {
                    createDirectoryResponseObject.empty();
                }, 2000);
                directories();
            });
            request.fail(function (jqXHR) {
                $(createDirectoryResponseObject).removeClass("success");
                $(createDirectoryResponseObject).addClass("error");
                let errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li>").text(name));
                $(createDirectoryResponseObject).append(errors);
                setTimeout(function() {
                    createDirectoryResponseObject.empty();
                }, 2000);
            });
        }
        function resetPasswordForm() {
            window.location.href = 'reset_password/mail';
        }
    </script>
@endsection

@section('body')
    <div class="login-page">
        <div class="form">
            <div id="form-response"></div>
            <form class="register-form">
                <h2>Sign Up</h2>
                <input id="create-name" type="text" placeholder="name"/>
                <input id="create-email" type="text" placeholder="email address"/>
                <input id="create-password" type="password" placeholder="password"/>
                <input id="create-password-confirmation" type="password" placeholder="password-confirmation"/>
                <button name="create-button" type="button" id="create-button" onclick="register()">create</button>
                <p class="message">Already registered? <a href="#">Sign In</a></p>
            </form>
            <form class="login-form">
                <h2>Sign In</h2>
                <input id="login-email" type="text" placeholder="email address" name="login-email"/>
                <input id="login-password" type="password" placeholder="password" name="login-password"/>
                <input id="login-captcha" type="text" placeholder="captcha" name="login-captcha"/>
                <img src="" id="login-captcha-img" onclick="getCaptcha()">
                <button name="login-button" type="button" id="login-button" onclick="login()">login</button>
                <p class="message">Not registered? <a href="#">Create an account</a></p>
                <p class="message" id="reset-password">Don't remember your password? <a href="#" onclick="resetPasswordForm()">Reset</a></p>
            </form>
        </div>
    </div>
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="w3-bar w3-black">
                <button class="w3-bar-item w3-button" onclick="runningProcess()">Running Processes</button>
                <button class="w3-bar-item w3-button" onclick="files()">Files</button>
                <button class="w3-bar-item w3-button" onclick="directories()">Directories</button>
                <button id="logout" style="float:right" class="w3-bar-item w3-button" onclick="logout()">Logout</button>
            </div>

            <div id="running-processes" class="w3-container w3-display-container tab">
                <h2>Running Processes</h2>
                <table id="running-processes-table">
                </table>
            </div>

            <div id="files" class="w3-container w3-display-container tab" style="display:none">
                <h2>Files</h2>
                <table id="files-table">
                </table>
                <div class="form-inline">
                    <input type="text" id="create-file-name" placeholder="File name" name="file-name">
                    <button name="file-button" type="button" id="create-file-button" onclick="createFile()">Create</button>
                </div>
                <div id="create-file-response"></div>
            </div>

            <div id="directories" class="w3-container w3-display-container tab" style="display:none">
                <h2>Directories</h2>
                <table id="directories-table">
                </table>
                <div class="form-inline">
                    <input type="text" id="create-directory-name" placeholder="Directory name" name="directory-name">
                    <button name="directory-button" type="button" id="create-directory-button" onclick="createDirectory()">Create</button>
                </div>
                <div id="create-directory-response"></div>
            </div>
        </div>
    </div>
@endsection
