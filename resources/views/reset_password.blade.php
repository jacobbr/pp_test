
@extends('layout.layout')

@section('scripts')
    <script>
        function resetPassword() {
            var resetPasswordEntry = $("#reset-password").val();
            var resetPasswordConfirmationEntry = $("#reset-password-confirmation").val();
            var resetPasswordResponseObject = $("#reset-password-response");
            $(resetPasswordResponseObject).empty();
            var request = $.ajax({
                url: "/api/v1.0/reset_password",
                method: "POST",
                data: {
                    user: '{{$user}}',
                    password: resetPasswordEntry,
                    password_confirmation: resetPasswordConfirmationEntry
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                datatype: "json"
            });
            request.done(function (response, textStatus, jqXHR) {
                $(resetPasswordResponseObject).removeClass("error");
                $(resetPasswordResponseObject).addClass("success");
                let success = ["Password Successfully Changed"];
                success = success.map((name) => $("<li class='li-response'>").text(name));
                $(resetPasswordResponseObject).append(success);
                setTimeout(function(){
                    window.location.href = '/';
                }, 3);
                runningProcess();
            });
            request.fail(function (jqXHR) {
                $(resetPasswordResponseObject).removeClass("success");
                $(resetPasswordResponseObject).addClass("error");
                let errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li class='li-response'>").text(name));
                $(resetPasswordResponseObject).append(errors);
            });
        }
    </script>
@endsection

@section('body')
    <div class="login-page">
        <div class="form">
            <div id="reset-password-response"></div>
            <form class="reset-password-form">
                <h2>Set New Password</h2>
                <input id="reset-password" type="password" placeholder="password"/>
                <input id="reset-password-confirmation" type="password" placeholder="password-confirmation"/>
                <button name="reset-password-button" type="button" id="reset-password-button" onclick="resetPassword()">Reset Password</button>
            </form>
        </div>
    </div>
@endsection
