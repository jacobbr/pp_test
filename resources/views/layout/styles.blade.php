<style>
    @import url(https://fonts.googleapis.com/css?family=Roboto:300);
    .login-page {
        width: 450px;
        padding: 8% 0 0;
        margin: auto;
    }
    .home-page {
        width: 360px;
        padding: 8% 0 0;
        margin: auto;
    }
    .form {
        animation-name: launch;
        animation-duration: 4s;
        position: relative;
        z-index: 1;
        background: #FFFFFF;
        max-width: 450px;
        margin: 0 auto 100px;
        padding: 45px;
        text-align: center;
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }
    .form input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
    }
    .form button {
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #FF6229;
        width: 100%;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
    }
    .form button:hover,.form button:active,.form button:focus {
        background: #FF5415;
    }
    .form .message {
        margin: 15px 0 0;
        color: #b3b3b3;
        font-size: 16px;
    }
    .form .message a {
        color: #FF7F50;
        text-decoration: none;
    }
    .form .login-form {
        display: none;
    }
    .container {
        position: relative;
        z-index: 1;
        max-width: 300px;
        margin: 0 auto;
    }
    .container:before, .container:after {
        content: "";
        display: block;
        clear: both;
    }
    .container .info {
        margin: 50px auto;
        text-align: center;
    }
    .container .info h1 {
        margin: 0 0 15px;
        padding: 0;
        font-size: 36px;
        font-weight: 300;
        color: #1a1a1a;
    }
    .container .info span {
        color: #4d4d4d;
        font-size: 12px;
    }
    .container .info span a {
        color: #000000;
        text-decoration: none;
    }
    .container .info span .fa {
        color: #FF7F50;
    }
    body {
        background: lightblue;
    }

    @media only screen and (max-width: 400px) {
        .login-page {
            width: 100%;
            height: 100%;
        }
    }

    #reset-password.error {
        color: red;
    }

    #reset-password-confirmation.error {
        color: red;
    }

    #send-reset-password-response {
        text-align: left;
        font-size: small;
        margin-bottom: 15px;
    }

    #reset-password-response.error {
        color: red;
    }

    #reset-password-response.success {
        color: green;
    }

    #send-reset-password-response.error {
        color: red;
    }

    #send-reset-password-response.success {
        color: green;
    }

    #reset-password-response {
        text-align: left;
        font-size: small;
        margin-bottom: 15px;
    }

    #reset-password-response.error {
        color: red;
    }

    #reset-password-response.success {
        color: green;
    }

    #create-name.error {
        color: red;
    }

    #create-password.error {
        color: red;
    }

    #create-password-confirmation.error {
        color: red;
    }

    #create-email.error {
        color: red;
    }

    #login-email.error {
        color: red;
    }

    #login-password.error {
        color: red;
    }

    #login-captcha.error {
        color: red;
    }

    #form-response {
        text-align: left;
        font-size: small;
        margin-bottom: 15px;
    }

    #form-response.error {
        color: red;
    }

    #form-response.success {
        color: green;
    }

    #create-file-response.success {
        color: green;
    }

    #create-file-response.error {
        color: red;
    }

    #create-directory-response.success {
        color: green;
    }

    #create-directory-response.error {
        color: red;
    }

    #login-captcha-img {
        margin-bottom: 10px;
    }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
    }

    .close:hover,
    .close:focus {
        background-color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #333;
    }

    li.nav-bar {
        float: left;
        border-right:1px solid #bbb;
    }

    li:last-child {
        border-right: none;
    }

    li a {
        display: block;
        color: white;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }

    li a:hover:not(.active) {
        background-color: #111;
    }

    .active {
        background-color: #04AA6D;
    }

    table {
        margin-top: 15px;
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    .form-inline {
        margin-top: 15px;
        display: flex;
        flex-flow: row wrap;
        align-items: center;
    }

    .form-inline label {
        margin: 5px 10px 5px 0;
    }

    .form-inline input {
        vertical-align: middle;
        margin: 5px 10px 5px 0;
        padding: 10px;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .form-inline button {
        padding: 10px 20px;
        background-color: dodgerblue;
        border: 1px solid #ddd;
        color: white;
        cursor: pointer;
    }

    .form-inline button:hover {
        background-color: royalblue;
    }

    @media (max-width: 800px) {
        .form-inline input {
            margin: 10px 0;
        }

        .form-inline {
            flex-direction: column;
            align-items: stretch;
        }
    }

</style>
