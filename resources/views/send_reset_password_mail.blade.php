@extends('layout.layout')

@section('scripts')
    <script>
        function sendResetPasswordMail() {
            var sendResetPasswordMailEntry = $("#send-reset-password-mail").val();
            var sendResetPasswordResponseObject = $("#send-reset-password-response");
            $(sendResetPasswordResponseObject).empty();
            let request;
            request = $.ajax({
                url: "/api/v1.0/reset_password/mail",
                method: "POST",
                data: {
                    email: sendResetPasswordMailEntry,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                datatype: "json"
            });

            request.done(function (response, textStatus, jqXHR) {
                $(sendResetPasswordResponseObject).removeClass("error");
                $(sendResetPasswordResponseObject).addClass("success");
                let success = ["Mail send successfully"];
                success = success.map((name) => $("<li class='li-response'>").text(name));
                $(sendResetPasswordResponseObject).append(success);
                setTimeout(function(){
                    window.location.href = '/';
                }, 3);
                runningProcess();
            });
            request.fail(function (jqXHR) {
                $(sendResetPasswordResponseObject).removeClass("success");
                $(sendResetPasswordResponseObject).addClass("error");
                let errors = $.parseJSON(jqXHR.responseText).errors;
                errors = errors.map((name) => $("<li class='li-response'>").text(name));
                $(sendResetPasswordResponseObject).append(errors);
            });
        }
    </script>
@endsection

@section('body')
    <div class="login-page">
        <div class="form">
            <div id="send-reset-password-response"></div>
            <form class="reset-password-form">
                <h2>Reset Password</h2>
                <input id="send-reset-password-mail" type="text" placeholder="email"/>
                <button name="send-reset-password-mail-button" type="button" id="send-reset-password-mail-button" onclick="sendResetPasswordMail()">Send mail</button>
            </form>
        </div>
    </div>
@endsection
