<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $reset_token = encrypt($notifiable->email.':'.env('PASSWORD_RESET_SECRET_KEY').':'.Carbon::now()->addMinutes(5)->timestamp);
        return (new MailMessage)
            ->greeting('تست پارس پک')
            ->from(env('MAIL_FROM_ADDRESS'),'تست پارس پک')
            ->template('email.reset_password')
            ->line(trans('email.password_request'))
            ->line(trans('email.click_password_link'))
            ->action(trans('email.reset_password_link'), env('APP_URL').'/reset_password?user='.$reset_token);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
