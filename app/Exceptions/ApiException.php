<?php

namespace App\Exceptions;

use App\Helpers\Responsetime;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class ApiException extends Exception
{

    private $validator;

    /**
     * Create a new exception instance.
     *
     * @param Validator $validator
     */
    public function __construct($validator)
    {
        parent::__construct();
        $this->validator = $validator;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  Request
     * @return Response
     */
    public function render()
    {
        $message = ['success' => false, 'error' => true, 'errors' => Arr::flatten($this->validator->getMessageBag()->getMessages()), 'responsetime' => Responsetime::GetResponseTime(microtime(true), microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
        return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
    }
}
