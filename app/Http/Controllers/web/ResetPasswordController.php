<?php

namespace App\Http\Controllers\web;

use App\Helpers\Responsetime;
use App\Http\Requests\User\AuthenticateRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Jobs\SendResetPasswordMailJob;
use App\Models\User;
use App\Notifications\ResetPassword;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;


class ResetPasswordController extends Controller
{
    public function reset_password() {
        $user = request()->get('user');
        return view('reset_password', compact('user'));
    }

    public function reset_password_mail() {
        return view('send_reset_password_mail');
    }
}
