<?php

namespace App\Http\Controllers\api;

use App\Helpers\Responsetime;
use App\Http\Requests\User\AuthenticateRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Jobs\SendResetPasswordMailJob;
use App\Models\User;
use App\Notifications\ResetPassword;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  User management
 *
 * APIs for managing users
 */

class AuthenticateController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'captcha', 'register', 'resetPasswordMail', 'resetPassword']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param AuthenticateRequest $request
     * @return JsonResponse
     *
     * @bodyParam  email email required The email of the user. Example: pourrahmanbr@gmail.com
     * @bodyParam  password password required The password of the user. Example: 123qaz!@#
     * @bodyParam  captcha string required The generated captcha. Example: 684214
     * @bodyParam  captcha_key string required The generated captcha key. Example: s^kcj83$ij23jk*nk%
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC92MS4wXC9hdXRoZW50aWNhdGUiLCJpYXQiOjE2MjU3MjkwMDQsImV4cCI6MTYyNTczMjYwNCwibmJmIjoxNjI1NzI5MDA0LCJqdGkiOiJuQVMzdFFQOW9TT2RaS3VvIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.00GANRnRCsDMtAuBYQRyvksK71UbxKDbelApHwVuxoY",
     *   "expire_at": "2021-07-08 08:23:24",
     *   "user": {
     *       "id": 1,
     *       "name": "Annamarie White",
     *       "email": "zconroy@example.org",
     *       "created_at": "2021-07-06T17:45:03.000000Z",
     *       "updated_at": "2021-07-06T17:45:03.000000Z"
     *   },
     *   "responsetime": "0:0:1.1111",
     *   "date": "1400/04/17"
     * }
     *
     */
    public function authenticate(AuthenticateRequest $request): JsonResponse
    {
        $start = microtime(true);

        $request->validated();

        $credentials = $request->only(['email', 'password']);

        try {
            if ((!$token = auth()->attempt($credentials))) {
                $message = ['success' => false, 'error' => true, 'errors' => [trans('auth.failed')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
                return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
            }
        } catch (JWTException $e) {
            $message = ['success' => false, 'error' => true, 'errors' => [trans('auth.can_not_create_token')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 500, [], JSON_UNESCAPED_UNICODE);
        }

        $message = ['success' => true, 'error' => false, 'token' => $token, 'expire_at' => date('Y-m-d H:i:s', strtotime('+' . config('jwt.ttl') . ' minutes')), 'user' => auth()->user(), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Create user
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     *
     * @bodyParam  name string required The name of the user. Example: jacob pourrahman
     * @bodyParam  email email required The email of the user. Example: pourrahmanbr@gmail.com
     * @bodyParam  password password required The password of the user. Example: 123qaz!@#
     * @bodyParam  password_confirmation password required The confirmation of password. Example: 123qaz!@#
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "item": {
     *       "name": "jacob pourrahman",
     *       "email": "pourrahmanbr@gmail.com",
     *       "updated_at": "2021-07-08T03:00:49.000000Z",
     *       "created_at": "2021-07-08T03:00:49.000000Z",
     *       "id": 21
     *   },
     *   "responsetime": "0:0:0.364",
     *   "date": "1400/4/17"
     * }
     *
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $start = microtime(true);

        $request->validated();

        DB::beginTransaction();

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->save();

        DB::commit();

        $message = ['success' => true, 'error' => false, 'item' => $user, 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     *
     * @authenticated
     *
     * @response {
        "success": true,
        "error": false,
        "item": "توکن با موفقیت غیر فعال شد",
        "responsetime": "0:0:0.22",
        "date": "1400/4/17"
     * }
     */
    public function logout(): JsonResponse
    {
        $start = microtime(true);

        try {
            auth()->logout();
            $message = ['success' => true, 'error' => false, 'item' => [trans('auth.token_destroyed')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
            return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);

        } catch (TokenInvalidException $e) {
            $message = ['success' => false, 'error' => true, 'errors' => [trans('auth.token_invalid')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 401, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Refresh token
     *
     * @return JsonResponse
     *
     * @authenticated
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC92MS4wXC9hdXRoZW50aWNhdGUiLCJpYXQiOjE2MjU3MjkwMDQsImV4cCI6MTYyNTczMjYwNCwibmJmIjoxNjI1NzI5MDA0LCJqdGkiOiJuQVMzdFFQOW9TT2RaS3VvIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.00GANRnRCsDMtAuBYQRyvksK71UbxKDbelApHwVuxoY",
     *   "expire_at": "2021-07-08 08:23:24",
     *   "user": {
     *       "id": 1,
     *       "name": "Annamarie White",
     *       "email": "zconroy@example.org",
     *       "created_at": "2021-07-06T17:45:03.000000Z",
     *       "updated_at": "2021-07-06T17:45:03.000000Z"
     *   },
     *   "responsetime": "0:0:1.1111",
     *   "date": "1400/04/17"
     * }
     *
     */
    public function refreshToken(): JsonResponse
    {
        $start = microtime(true);

        $token = JWTAuth::getToken();

        if (!$token) {
            $message = ['success' => false, 'error' => true, 'errors' => [trans('auth.token_not_provided')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 401, [], JSON_UNESCAPED_UNICODE);
        }

        try {
            $token = auth()->refresh();

        } catch (TokenInvalidException $e) {

            $message = ['success' => false, 'error' => true, 'errors' => [trans('auth.token_invalid')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 401, [], JSON_UNESCAPED_UNICODE);
        }

        $user = JWTAuth::toUser($token);

        $message = ['success' => true, 'error' => false, 'token' => $token, 'user' => $user, 'expire_at' => date('Y-m-d H:i:s', strtotime('+' . config('jwt.ttl') . ' minutes')), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Get captcha
     *
     * @return JsonResponse
     *
     *
     * @response  {
     *   "sensitive": false,
     *   "key": "$2y$10$HlrIxV9.133JJcyAB8O.NO/tHluFD0TGRzaDXRAoeuDbSxvY1lx8.",
     *   "img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAkCAYAAABCKP5eAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAaqElEQVR4nMWbeZRb1Z3nP0/vad9KJalUi6tc3o1t4gUwARIDh8VNJ2wJwYQEs4QmNIRAOhACnc4wdLqnSSYcOluzBBhsCITGdIAJJB0CASdmscHBBgNey64qV6lU2ksqSU96b/6Q760nlcokPZkz9xwdveWuv+1+f7/ffYqu66ZpmthsNhRFkb8/t5imCYCiKJim2XB/tP5EXVHHMAz5bmBggPb2dgKBADabTfZnGIYc52jFWudPXZdpmhiGgZUmhmFQrVax2WxomtawPtG3mLt4Z7PZUFUVwzCo1Wo4HI6PHPsvWQSNNLEoMdH/CnOt5aOI3qq0GrNSqVCtVrHb7Q3vmwn7XxnvaMVKi1qtJpmsaRqlUol8Po+u6+i6jmmaqKqK3W5H0zQcDgcul0vOSxJZ0/6ic/xT16EoCpqQNrGQv2TnM923qg9TjFYUBV3XAVBVdRojBeH+nDm30rhWxcoc0X+5XCaZTJLL5ajVaqiq2qDdtVoN0zTRNA2/309bWxtutxtACsn/j2IYxhSDxQOxSOt/MwNmurdeH43Jze2b2yqKMs0kCqZa29RqtRkXZzXpou9mbT/afGw2G4ZhkEqlSKVSKIpCW1sbfr8fh8MhtdI0TXRdp1wuk8vlSKfT5HI5otEooVBImumjrf//xb3NZqNWq6Gpqjpt0a1MovXZTO+PpiUztbe2tbav1WooitKgwVaM8FHbiRAI65paza95fUKwTNMkkUiQTCbx+XxEIhE8Hk/DuKJPVVVxOBx4PB6CwSDJZJJ0Oo2iKJLJfwr9/tz3y77wMDsfvaLle8FkrZUWNRO0eTBrR0fTgpn2yOY9v1m6W4Gc5netGC7qWNdgGIY0qdb31jEEQ63jpFIpxsfHpSZqmtawNmt70cZms+H3+3E6nbJ9MBhssEiivrA+QsGagakYo1kBBWMB3n3sygbrYFUURVHI5/N1E21lSHLPAcY/3E908TzCC+bMuH+2Ms/N75rvhw6k2LNrjFPOmo/drja0Fxpr1Q7DMNB1XS6ieVyxDwtBsJplAZKs5r2ZeOJ5rVaT16qqkslkSCaTBINBotHoNCEDGvbV5m1OVVWCwSATExNkMhkikUgDI8VcxdysjGxeq5X+VsbOxAtRv1wuMz4+jiYGEp0+e93fM/bebtZ88zpO+upVfzZzrcWqSQBPPLCVzb/ew6aH3+J7Gz6Hy22nUqnywTsjHD6UwRdwsWRFF+1Rn9zbqtXqUccRIEa4JVZ3byKxj8Pvvkh25APMmo7DHaS9fyU9x/4VqtPX0vQXi0XS6TQul4uuri4UReHkJy5jeCJOyBVgx/qnG3BGs/aoqoqu67jdbvx+P8lkklAo1GCRrEJSq9VY94XvYlMU1p69iquvWisZLWg9E2Ob6SuuK5UKg4OD5HK5RhNtmibh+f2Mvbeb5J6Bo+6j1g5nKs31e+e0Y1MVBg+kef7JnVT1Gv+xcTuTBd3SBlafOofzL1+EoUxn7kym2Ipk86MfsO2JW4h/uLnlvDSnl2POvoml59zM7t/9lN0v/QRfuI/+ky9H6zoBVVXp6OiQzGq1LitRdV2nVCpRKpWkNfD7/fh8PulWOZ1OOW/rOsplnVc3v4uu1/D6XFx91VqJzBVFYfn6DdMYK4pA9KJfm82GruvE43EymUwdRQvCCC1on98PQHLvgRkZ11xmQsXN5eKrj2f3e3G2bR7gZ//2BoZpsnpNP0tWdqMoCu//cYTXf7efN353gJ3bhrjyG8fS329MM4dWQjfv3ftfe5ytj34F05gZYVfLBXY+90+M73sNd/tsJhL7mUjsp23hGYQ6VhGJRHC5XFSrVRwOB61WJcYtFArE43Hy+by0IKZpksvl8Hq9klmC8aJOtVoln8/zyqs70PX6XI9Z1EU+n8fj8bDqyseA6VqbTOZIJLIsXtw7Dezpuk4ul2N8fFzu8ZrYgwSxIgvn1jvaMzBtUa3242YX6SNN+pH6bq+Df7z3fPoXRGS9v754Ge/vGOaum/+TfLbM4z/axXEnLsbpcrQEc8JXFc8P73yBNzdeJ8cAaJ+9ks6lZ+ML91KrFEgd/CPDO35FpZhmZNdLdZNxpNjcEal5rUAmJg1jZjIZnj3O5IT/Xaajo4NIJIKmaei6Tj6fJ51Ok8/npaZpmoZhGOTzecbGxigWi2zdtld2P39emJP/dhMAb9x/MXa7fRqtn3r6D3zjtocJBjz89j//mblzYpIWyWSS0dFRSqWSnLcGNGzw4QX9AOjFSXIjYwS6OuQA4x/uJ7X/IGbNIDSnl46lCxuYejRELhgzNJABYNGxsQbmCjM7d1GEa279BN+//bekxkq8+qt9nHn+YqkZgLwWC1MUhUohxfYn/k4yV3N6OenK++hdeZ5EsKqq1jW9prPr1/fw3vP/k5o+KefgDHbj8XimhUVblUKhwOjoKBCjp6cHr9cr12+32wmHw7hcLlwuF1acUywWGRsbQ9M0+vr62LP3lwD0rj2HO54e5oV/OZNUKkU6naazs1MKlGj/2usfAFDRq/TP7pDCUygUGBsbY2JiogHxa5VKBYfDITsIz+uvS7Vpkt5/EH9nlD9ufJrXfvAQueHRhkWGF87h7H+5jd4TV87ol1oBSa1qMDaSA6CnP9SScAAnrOkn3OEhOVZk6ysHOfP8xQ19NQsPwP7ND1EppDjCGU69/kliiz7ZAFbE/FTNwcc+fSuROcfz0j3nyz7doR6pNTNtNwKhplIpGewIBAINAieKx+ORES3xrlgsAtDe3o7P52PrW3voXXsOcwqH+PcnbpWoOpfL0dHRMU1B3njzQwBWrpiHooCqalLYCoVCgyIoioJtz549VKvVKSJoKqHZswDIDo7wm9vv4te3/jOF8RSzP7ma5V+8kMXnnYXD5yW5+wA/X3cdA6+8PiMxrNcjg1mMWn3w3jmNDG72Q/vmtwMwMpid5mu20qqB1zbK6/7V6+hcvKYBrDQDQtM0qRRz8l5zt+H2BhvixtOYrNSBTTqdZnJykt7e3ml1rXhBuENi7FKpxMTEBOFwGLfbzfL1G/CfuIbBX7/Axz++WLpp1oSGWK9pmgwNJzk8Uhfij69ehM1mo1KpMDQ0xPj4eIP7JSyiZpomQ0ND9Pf3S3PXvqCf9MAgb/+vJxl9532Wfe5TnPHfv46rLSAnX0xl2HTF33F42w6e+8o/8OUtv8AV9E/nMlOaN3wwI5/19IdaBjlEsdnqz2pNfiLQYLYAiukhSrm4fD/vlPWyP6t7Yp2PoigUUoPymSPQhaZpHxk31nWdLcPbeebwq7z75j5u5D5WbLiIj0UX8oXFn2LtnFMaAjRWC1IqldA0jTU3PAPA18+exU1ff0AyTFVVstksyWSSzs7OacIizDPAiasXUa1WGRwcJJPJyGhZM7LW+vv7GRgYIJPJEAjUGRie38++32xm9J33mbV6BZ/+wZ0NxDFNE3coyAUP3MUDp3yGyVSGtx56glO+9jcNxGiOzgzuT8l3vXPb5fOd37tmGiEvnAUXXlK/Hrh361GJDrB84aXyOty/alo0rFUpJA/Ja2ewexpItM5f3H/njfvZsPu5hn6SpQwvD77Jy4Nv8rmFa7n7tFumjV+r1Tjtxnq7tx66FFVVuf6r90oaHHfcfLLZLPF4HI/HQ1tbmwRlwgK9ufVDWf/44+YRj8cZHx+vu0OaVtfYI20Mw8Dr9aK53W4ikQjj4+N4PB4cDgfhI64SwLKLPz2NMGLRvo4Iyy76a7Zv2MS7Tz3fwGDr3gt10yUAVjDkxh9wyffLbr5P5lxrtRrxw1m+dskmTBNOOrubL16/mmAw2GCurMBuaPszbNvw5fo4mhN+1JKf04qbRsGIP/It4i3qPYAP8NVvErv4PPOmXv7tN3khZ7nftpd3t3255Xi/uuvbOJ1OyXSxny5d0ketWo88uVwuYrFYA5AU1kBo8KKF3RSLOUZHR6VZF1uRNf8ci8XqbpLP56NSqVAoFHA6nUQWzZWTCvR0yutmiQaYe8YpbN+wifT+QxQSKbzR9pZxVYChgTQAtZpBYjRPJOaT78T+UavV+I9H3pGezvFreiiVSvj9fjkH63zqxJoyZUatwsKv3I2paGiaJvczMYZVs56/82TSQzsBmH/Ot+g54Qt0dnZKEyeCGGv+/QqGJuqsX9I2lx+c9A1mR2fhdDrZOD/DOa9X2Tz8Nrdsu4eKoWO3aWy++BG6/R187LJHANj600vY+6MbARBQdeM6gDYgy+GHbgOgAuxvKR5w37nAuW1AgezPv8N9m30z1JwqmqqquFwuHA6HjAaF58+ZIqIF0LRicOfyJfI68cFevNHVsi7QADIOH9mDJ3Jlbr/6aW7+H2tZsHQKKdZqNZ5+ZDuvPF/3DU8+cy4Ll3aRy+XQdX3aqQjRzhudEkhMk8S+N+k65tRpKcPm6wmLifaFe9F1ncnJSenyNKcj7TaN766+kbC7Dbvdzvj4OKCRzWZZ1baIdbPPYuOB59GNKn913QsAnJivC9BX1u1EWoH/y7LglOWcftpirlkzJYRCKGu1Gm1tbfT399fTmmLRdrudarWKYRi4gn48kRDF8TT5kbEjdGsdQfJ1RLDZNQy9SiGRnBYXFW3HRnKUJuthv+WrZ7HzrWG+ccVTLFnZxcJlMfRKlT++PsjwwSwAvXNDXHHTahyuevC/UCjgcrkaEuiqqlKpVPDHFuLwtFEp1gVo90s/oXvJaTMSSFEUJjIJ9Mn6WC/Hr+Hl778FvNWy/lI6WUrdkv3wuYcaXy6DV78+dbuW5bzhP5YT8zu5d9Ndcm8ESCQSAEQiEZ56egvXXv/jep/3XMEF530Cn8/XcBxJ+O2KonDDTfey8bGXAfjp+jMk/QVzRZjS7XbT1dUlT5ZIBmuaRqVSkagvPH8OxfE0iQ/2zhhvFhOxaSqGXm2ICkGjyzA8MIWgL1y/ikuuWc2P/+lldm0fYdf2EUufdc1d/9UTcXvrR2Hcbjf5fJ47b/jXlvOol4unLuPw2Is3H6WuKHVwd3rsfj579yFyRZ1isUg4HGZiYoJgMIjdbpcmOuQM8IfPPMLIyAgTExP09fXx/Ikqn//Ah6qqLF+/AYDMmkd5xxNuiLYJGhcKBWq1Gk7nVJQqmawLr9g+VFWVyqYoCql0jqd/sQWAUMjDnP5YQ74c6prrcrno6OhoEBTJ4GbzG1k4h8HX3+bQH7YB03O4QoJK2TzVyTIA3khoxri01UWa1R8i0unjnscv5trP3jqN7B/u2s/fX/vitOe33X0dsdhUaE4sym63U0iP8MKdq6mW8rL+glO/xMfOvQ13sBFHDO34FVsfv4VicgAAzeXH5QthqpOUSiWJTKvVKrFYrGEOHo+H7u464vZ6vXxxD5Kxz9y5hsu2fJtMEXKVAqqqUi6XcTqdmKaJw+Egl8tRLpdZtXKepPujj2/mmqs/jculNgRLxPtv3v4whUKdxkuX9ABTaUVheRVFIRwOE41GpcuUTCbrfrBpmjKcJxgj9uHE+3uJv7eb6DHzpwX3Afb+pp6xsdk1OlcsbSCG1UwPHagDLIdLIxzzUq1W0XWduzf+twa0aM0SWVFzLpcjm82Sy+Xw+eqpPofDIc2YOxhj9WU/5LUHvyQTDXteeZB9v99A++wVeNt7qeklkgNvM5ltjMj5wn0AOJ1OAoEAiUQCh8PBxMQEHR0dDXUNw8DtdmMYhmTstgc/z+DgYF2r1CN54SPrsNvtU66l213PduXz9HTHWPe5T/DEk5vZu3eU08+6nVtvuYizzliJ210/aDg2luGOf3yUJ5/aIsdfcky3DEwJPihK/ThRJBKRWGpycpKhoSE0QUCrLQdoPxKTBnjxW99j3c9/jNoU/M6PJth810/qA194Dg6vZ8aExOCBug88a3YbtVpNnkxsdQRWtLcm6gOBAPl8nlQqhcPhwO12y4yPMGv9x38GVdV4Y8MNVIp1gTJqOuP7tzK+f8qXVu1uPO295OO7AfCGZ8vxPR6P9CpEYsBaBNI/7qqf8c6G9TKsqCgK7e3tpMv1fT3g9ErGWk9Xut1uGQn7/ne/RD5f5JcvvMWHu4e56m/+FUVR6OoMYbMpDB9OYZomwaCbbLYeM1+2pLvBharVavh8Pjo6OvB4PFSrVSqVCqOjo/XTnoK5k5OTRCKRKQbPmy0XNfTGdh4770usumodoTm9VPIFhre9w/YNm5hMZfB3dXDqt244aqBAaHD37DbK5bKUQqGBzQIh9/cjWiwC+MPDw6TTaXw+n/SbHQ6HXHTvyvOIzP04u1++lwOv/Yxi5vAUYx1u+ladz7JPfZPtT/+DZLCnfZYUcFVVCYfDjI2NUSqVyGQyDfNbcXk9JLpj4+WIOL7D4UDXdT5MHCBXKQCwoG12g2US6woEAjLFGIvFuP/frueXz2/hF8++zebfv09+YlKGI/v6wnz2/FWk0gUeeXQLTqfGnP5IQ1jXbrfT09OD3+/HMAwqlQojIyMYhkFPT099Dx4bG0NRFBkYN02TQE8ndo8bvTjJ6Xd8jS13P8DzN91Bc+k96TjOuefbuNoC05gLRxBrvkw2XZfAWI9PAgRo1FJRrBotNMY0TVwuF+3t7aRSKTKZDG1tdXfFeoqjbq47WH7Bt1l+wbcppoaYzMVR7U7aupdgU+uRHmsUyxXsluFQ4VGEw2FKpRJjY2N11+PVL2ICz/7wTPoCXXILAaRF+emuZ2SfJ3YskwJqLQ6HQwrq8PAwc+fO5cwzjuOUk5cQCAQYHBzj4MHD2B0mLmf9VOn9D74KwKIFndjtU369YRj09vbKvDNAPB6XcXK/34+WSCTIZrPMnTt32sG09rl9xN/9kFXrL2LFpRfwwXMvMrZrD0a1RmBWJ7M/sZqOZQunMUcUIVHlUolLrjmOwYE085dGZow/tyoitlqpVNA0jVgshmEYHD58GKfTSVtbm8QQ1vyp6N8b7sUb7m1w2UzTpJCcikOXFJ/M44o+XC4XfX19XHjH74FzyKx5FIBrf7uFh8/+Dp2+iBQsu93OjsI+Ng29VGeizc66xefIZIDYF4UA+nw+enp6SCQSHDx4EKfTSTabJZVKUSqVCAZVmXSo1Wp88pQFVCo6c/qj2O12KpWKPHUSjUZljHt0dJRisUhnZyc+X93nVnbs2GFGo1Gi0eg0Bu984lkq+QlWX3tZAzPERK1m1sow8a5Wq0m43yzNrRjbivFCM6vVKk6nU8ZaR0ZGKBQK9Pb2EggEGtpZkb71FKYo1UqRp27slvcL1j3E7GVrCIVCDaBoxeUbee3eizh105WMldMoKJiY+Owezpt3OovaZmMCb8Z38sKB32NSH+PK2eeyft6ncLvd8jit8Eth6miuYRjE43HS6TTFYlE+E/S1WhWhoWK76ujoqIcij9Ajm80yOjpKOByWiQrDMNCi0ShtbW2yE2vaa/mlF7TUtOajmuK9cF2q1aoEP82a0+ySNWeUmhkt5uVwOCTxnU4ns2bNYu/evYyPjx8BIsEZ+2oGcJPp4QbBisxaRCKRwOVyyYxStVpl24Ofr0eD1HoKcZ67hxXBBTw1+jI/++CX0wQUYN38tVy3ZB2ZTIZEIiEPvjudTux2O06nk1KpJI8DFYtFKpWKDIZYtxq73d5w2rNaraKqKpFIhFgshtPppFwuoygK2WwWn89HOByW61dVFS0cDssoCNCQMrPuhVbGiMlYMyWCqVaitmJkK0B1tH+xOIfDIc2hkOyFCxeyb98+Dh+uAymrJos+rGlF0V/RkiZU7W565x7DyMgIhw8flqczRAChXC5PAU93kEu7zuZYzzxezG7jg8JBMpU8PruHVR3HcNkx53Ja7wkARKNRKpUKABMTE+i6Ls9l2e127Ha7fFapVOQ3WNVqVZ7I0HVdmmmBwsPhMF1dXTgcDlk3Ho+jaRrRaBSn0ymzSUe+DNFafuRlJbK4Fs4/NJ7fbWaoNSFvBVOtTHSzabdaEEVRcDqd0n1rBlOGYdDX18fo6CgjIyNUq1VCoVCDQLVywfKJAXnvaZ8lTd7o6CiHDh0iFAoRjUalMP3hko11zZ+clG7aMd7ZOBwOQqEQwWAQr9crAZ8oLpdL/lvpINB5Pp+nWCxOHSU6QjerAgnN1TSNUCgkfV2RIhTnvmKxmPTPRdrQZrNNnaoUxGjes8TA1glYT+iLvbb5YHhzad7fBbGb90qoWxG73S6BhrWuVShUVcXpdNLZ2cno6CjxeBzDMBrQtcADVtBi1WDhA7tcLnp6ekgmk6RSKSYmJojFYtIds+59Xq9XgqLR0VFSqRQ+nw+/3y+P6ViZLRSjVCqRzWbJZDKUy+UGb2ImD0TQKxgM0t3djdPplMBTBIACgQB+v7/hjLgo2uTkZEs3xeoGyMpHTIeYsPVfMLFZW5rvrWM1B8zFR12apk37nqeV8AjhdDgcdHd3E4/HpSZHo1EpHELiBbgppKZcJH90dkMMuKOjA5fLRTwe59ChQwQCAUKhEG63W87J6/XKoEUul6NQKMhIm91ul3hB0KNWq8kz0yKAIgTYuq2J7ciqTLquE4vFiEajMptmtWR+v19+Py0YbM2CaQJON2uVsOPiTK9V08VzUb/5aMmfUqxCIb6vFf/NpryZsVYNFhqqaZrMoiQSCSqVCu3t7Xg8HtlGIE6woTm9VMsFvO29DVkbERny+/3yE5bh4WGcTider1eaYo/Hg9frlW6K+InvmsXaxL9gTrlcZnJyUn61Id5bLaDV62hvb6enp0dm0gRvhNsYDoelEAlwaA05K5VKxbQSUTDPapabzepHleZ9VSyiGUCJyJHb7W4w1819tOpbvBO+q5izzWajWCwyMjKCruv4/X6plc3gsFxIoSgqmssv+7aCSPGsUCjIn8ArwuKIXLpYq/Vjtmq1Srlclp+XTkxMUC6XZd+tLJ/ox+Vy4ff76e7ubjiZKYIvk5OTBAIBOjs7JXOt8QCp5alUyhSNre6PdU+0amkrlDsT062mxhqSFForAFSr9q384ebxxbWwLla3Qtd1xsfHSSaTeL1ewuGwNK1WF89KZNFXM9GtwlSpVCiVShQKBel5CC/CqjmC2SKMKgCV2Aqsn9oIjCDMq6IoRKNRuru7pTCJ5+l0mgMHDsgDECtWrGjY64EGIfw/WFAnvxhe9csAAAAASUVORK5CYII="
     * }
     */
    public function captcha()
    {
        return app('captcha')->create('default', true);
    }

    /**
     * Sent reset password mail
     *
     * @return JsonResponse
     *
     * @bodyParam  email email required The email of the user. Example: pourrahmanbr@gmail.com
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "item": "لینک ریست رمز عبور به ایمیل شما ارسال شد",
     *   "responsetime": "0:0:6.6754",
     *   "date": "1400/04/17"
     * }
     */
    public function resetPasswordMail(): JsonResponse
    {
        $start = microtime(true);
        $validator = Validator::make(request()->all(),
            [
                'email' => 'required|email|exists:users,email',
            ]);
        if ($validator->fails()) {
            $message = ['success' => false, 'error' => true, 'errors' => Arr::flatten($validator->getMessageBag()->getMessages()), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
        }

        $user = User::where('email', request()->get('email'))->first();

        $this->dispatch(new SendResetPasswordMailJob($user));

        $message = ['success' => true, 'error' => false, 'item' => trans('email.reset_password_sent'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Password reset
     *
     * @return JsonResponse
     *
     * @bodyParam  user string required The token of the user that sent to her via email. Example: eyJpdiI6InZiazQ2RHo2YTlBcFpQT2RLcDV6cWc9PSIsInZhbHVlIjoieXJlTVJ3WWoxZDVXS3l1WTZ6MzM1MXNOaTBmUFNWTlFDR3JmR3VMbkYxcWNMbDlQSUJ2SFI4dm51Zmc5MGsvQ0JUbjNTUUU3emNUSnV4bm51cXNYNVE9PSIsIm1hYyI6ImUyNGM5YzMwZDVlM2Q4ZjIxOTAzMzYwZjFkZTEzYjQ2ZDkxZjA5NDM3ZDFkMTVkNzdkYzUwZDYzMWJlMDViY2MifQ==
     * @bodyParam  new_password password required The new_password of the user. Example: !1dk[h',io,kiHsj
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "item": "رمز عبور شما با موفقیت تغییر کرد.",
     *   "responsetime": "0:0:0.193",
     *   "date": "1400/04/17"
     * }
     */
    public function resetPassword(): JsonResponse
    {
        $start = microtime(true);
        $validator = Validator::make(Request::all(),
            [
                'user' => 'required',
                'password' => 'required|confirmed|min:6|max:20|regex:/^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                'password_confirmation' => 'required'
            ]);
        if ($validator->fails()) {
            $message = ['success' => false, 'error' => true, 'errors' => Arr::flatten($validator->getMessageBag()->getMessages()), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
            return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            try {
                $decrypted_hash = explode(':', decrypt(request()->get('user')));
            } catch (\Exception $e) {
                $message = ['success' => false, 'error' => true, 'errors' => [trans('password.invalid_link')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
                return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
            }
            if ($decrypted_hash[2] < Carbon::now()->timestamp) {
                $message = ['success' => false, 'error' => true, 'errors' => [trans('password.expired_link')], 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
                return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
            }

            $updated = User::query()->where('email', $decrypted_hash[0])
                ->update(['password' => bcrypt(request()->get('new_password'))]);

            if ($updated) {
                $message = ['success' => true, 'error' => false, 'item' => trans('passwords.success_reset'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d')];
                return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
            } else {
                $message = ['success' => false, 'error' => true, 'errors' => trans('passwords.reset_error'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/m/d H:i:s')];
                return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }
}
