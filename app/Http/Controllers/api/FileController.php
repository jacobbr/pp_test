<?php

namespace App\Http\Controllers\api;

use App\Helpers\Responsetime;
use App\Http\Requests\File\DirectoryRequest;
use App\Http\Requests\File\FileRequest;
use App\Models\File;
use App\Models\Process;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

/**
 * @group  File management
 *
 * APIs for managing files
 *
 * @authenticated
 */

class FileController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * List of files.
     *
     * @return JsonResponse
     *
     * @authenticated
     *
     * @response {
     *   "success": true,
     *   "error": false,
     *   "item": [
     *       "file1",
     *       "file2"
     *   ],
     *   "responsetime": "0:0:0.4",
     *   "date": "1400/4/17"
     * }
     */
    public function getAllFiles(): JsonResponse
    {

        $start = microtime(true);

        $files = File::GetAllFiles();

        $message = ['success' => true, 'error' => false, 'item' => $files, 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * List of directories.
     *
     * @return JsonResponse
     *
     * @authenticated
     *
     * @response {
     *   "success": true,
     *   "error": false,
     *   "item": [
     *       "directory1",
     *       "directory2"
     *   ],
     *   "responsetime": "0:0:0.4",
     *   "date": "1400/4/17"
     * }
     */
    public function getAllDirectories(): JsonResponse
    {

        $start = microtime(true);

        $directories = File::GetAllDirectories();

        $message = ['success' => true, 'error' => false, 'item' => $directories, 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Create file
     *
     * @return JsonResponse
     *
     * @bodyParam  name string required The file name. Example: file1
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "item": "فایل با موفقیت ایجاد شد",
     *   "responsetime": "0:0:0.0",
     *   "date": "1400/4/17"
     * }
     */
    public function createFile(FileRequest $request): JsonResponse
    {

        $start = microtime(true);

        $request->validated();

        try {
            File::makeFile($request->get('name'));
        } catch (\Exception $exception) {
            $message = ['success' => true, 'error' => false, 'item' => trans('file.error_in_create_file'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
            return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
        }
        $message = ['success' => true, 'error' => false, 'item' => trans('file.create_file'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Create directory
     *
     * @return JsonResponse
     *
     * @bodyParam  name string required The file name. Example: directory1
     *
     * @response  {
     *   "success": true,
     *   "error": false,
     *   "item": "پوشه با موفقیت ایجاد شد",
     *   "responsetime": "0:0:0.0",
     *   "date": "1400/4/17"
     * }
     */
    public function createDirectory(DirectoryRequest $request): JsonResponse
    {

        $start = microtime(true);

        $request->validated();

        try {
            File::makeDirectory($request->get('name'));
        } catch (\Exception $exception) {
            $message = ['success' => true, 'error' => false, 'item' => trans('file.error_in_create_directory'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
            return response()->json($message, 400, [], JSON_UNESCAPED_UNICODE);
        }

        $message = ['success' => true, 'error' => false, 'item' => trans('file.create_directory'), 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
