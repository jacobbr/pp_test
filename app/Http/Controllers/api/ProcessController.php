<?php

namespace App\Http\Controllers\api;

use App\Helpers\Responsetime;
use App\Http\Requests\User\AuthenticateRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Models\Process;
use App\Models\User;
use App\Notifications\ResetPassword;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @group  Process management
 *
 * APIs for managing processes
 *
 * @authenticated
 */
class ProcessController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * List of running process.
     *
     * @return JsonResponse
     *
     *
     * @response {
     *   "success": true,
     *   "error": false,
     *   "item": [
     *       {
     *           "USER": "jacob",
     *           "PID": "1",
     *           "%CPU": "0.0",
     *           "%MEM": "0.2",
     *           "VSZ": "85040",
     *           "RSS": "19256",
     *           "TTY": "?",
     *           "STAT": "Ss",
     *           "START": "07:22",
     *           "TIME": "0:00",
     *           "COMMAND": "php-fpm: master process (/usr/local/etc/php-fpm.conf)"
     *       },
     *       {
     *          "USER": "jacob",
     *           "PID": "7",
     *           "%CPU": "0.1",
     *           "%MEM": "0.5",
     *           "VSZ": "105252",
     *           "RSS": "42432",
     *           "TTY": "?",
     *           "STAT": "S",
     *           "START": "07:22",
     *           "TIME": "0:05",
     *           "COMMAND": "php-fpm: pool www"
     *       }
     *   ],
     *   "responsetime": "0:0:0.262",
     *   "date": "1400/4/17"
     * }
     */
    public function index(): JsonResponse
    {

        $start = microtime(true);

        $process = Process::GetAll();

        $message = ['success' => true, 'error' => false, 'item' => $process, 'responsetime' => Responsetime::GetResponseTime($start, microtime(true)), 'date' => jdate('Y/n/d')];
        return response()->json($message, 200, [], JSON_UNESCAPED_UNICODE);


    }
}
