<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CommandInjectionProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $output = array_map(function ($item) {
            return str_replace("'", "", escapeshellcmd($item));
        }, $request->all());

        return $next($request->merge($output));
    }
}
