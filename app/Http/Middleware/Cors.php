<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $allowed_origin = 'http://localhost:8000';
        if (in_array($request->header('Origin'),[
            'http://localhost:8000',
            'http://130.185.75.166:8000'
        ])) $allowed_origin = $request->header('Origin');

        if ($request->hasCookie('access_token'))
            $request->headers->set('Authorization', 'Bearer '.$request->cookie('access_token'));
        return $next($request)
            ->header('Access-Control-Allow-Origin', $allowed_origin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
    }
}
