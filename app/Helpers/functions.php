<?php

namespace App\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

function paginate($items, $total, $per_page, $appends = []): LengthAwarePaginator
{
    $page = request()->get('page', 1);
    $offset = ($page * $per_page) - $per_page;
    $paginator = new LengthAwarePaginator(
        array_slice($items, $offset, $per_page),
        $total,
        $per_page,
        Paginator::resolveCurrentPage(),
        ['path' => Paginator::resolveCurrentPath()]
    );
    return $paginator->appends($appends);
}
