<?php
namespace App\Helpers;

class Responsetime
{
    static function GetResponseTime($starttime, $endtime)
    {
        $duration = $endtime - $starttime;
        $miliseconds = round($duration,3)*1000;
        $hours = (int)($duration / 60 / 60);
        $minutes = (int)($duration / 60) - $hours * 60;
        $seconds = (int)$duration - $hours * 60 * 60 - $minutes * 60;
        return $hours . ":" . $minutes . ":" . $seconds. '.'.$miliseconds;
    }
}