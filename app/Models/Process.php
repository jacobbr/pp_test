<?php


namespace App\Models;


use Illuminate\Pagination\LengthAwarePaginator;
use phpDocumentor\Reflection\Types\This;

class Process
{
    private static $processes;

    private function __construct()
    {
        //
    }

    public static function init()
    {
        try {
            $exec_string = 'ps -aux 2>&1';
            exec($exec_string, $output);
            $pattern = '/(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*?)[\s]+(.*)/';

            preg_match($pattern, array_shift($output), $keys);
            array_shift($keys);

            self::$processes = array_map(function ($item) use ($keys, $pattern){
                preg_match($pattern, $item, $values);
                array_shift($values);
                return (object) array_combine($keys, $values);
            }, $output);
        } catch (\Throwable $throwable) {
            self::$processes = [];
        }

    }

    public static function GetAll() {
        self::init();
        return self::$processes;
    }

    public static function Paginate($per_page = 10): LengthAwarePaginator
    {
        self::init();
        return \App\Helpers\paginate(self::$processes, count(self::$processes),$per_page);
    }
}
