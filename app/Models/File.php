<?php


namespace App\Models;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class File
{
    /**
     * @var
     */
    private static $files;

    /**
     * @var
     */
    private static $directories;

    /**
     * File constructor.
     */
    private function __construct()
    {
        //
    }

    /**
     *
     */
    public static function init()
    {
        self::$files = Storage::files();
        self::$directories = Storage::directories();
    }

    /**
     * @return mixed
     */
    public static function GetAllFiles() {
        self::init();
        return self::$files;
    }

    /**
     * @return mixed
     */
    public static function PaginateAllFiles($per_page = 10): LengthAwarePaginator {
        self::init();
        return \App\Helpers\paginate(self::$files, count(self::$files),$per_page);
    }

    /**
     * @return mixed
     */
    public static function GetAllDirectories() {
        self::init();
        return self::$directories;
    }

    /**
     * @return mixed
     */
    public static function PaginateAllDirectories($per_page = 10): LengthAwarePaginator {
        self::init();
        return \App\Helpers\paginate(self::$directories, count(self::$directories),$per_page);
    }

    /**
     * @param $name
     * @return mixed
     */
    public static function makeDirectory($name) {
        Storage::makeDirectory($name);
    }


    /**
     * @param $name
     * @return mixed
     */
    public static function makeFile($name) {
        Storage::put($name, 'Contents');
    }

}
