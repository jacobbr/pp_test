<?php

namespace Tests\Feature\Process;

use App\Models\Process;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\Validator;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;


class ProcessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_get_list_of_process()
    {

        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'bearer' . JWTAuth::fromUser($user)
        ])->get('api/v1.0/process');

        $response->assertStatus(200);
    }


    /** @test */
    public function unauthenticated_user_cant_get_list_of_process()
    {
        $response = $this->get('api/v1.0/process');

        $response->assertStatus(401);
    }

    // Omitted some tests due to lack of time
}
