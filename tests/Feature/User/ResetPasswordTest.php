<?php

namespace Tests\Feature\User;

use App\Jobs\SendResetPasswordMailJob;
use App\Models\User;
use App\Notifications\ResetPassword;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Str;
use Mews\Captcha\Captcha;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function send_reset_password_mail()
    {
        Queue::fake();

        $user = User::factory()->create();

        $response = $this->post('/api/v1.0/reset_password/mail', [
            'email' => $user->email,
        ]);

        Queue::assertPushed(SendResetPasswordMailJob::class);

        $response->assertOk();
    }

    /** @test */
    public function password_can_be_reset_after_link_sent()
    {
        $user = User::factory()->create();

        $reset_token = encrypt($user->email.':'.env('PASSWORD_RESET_SECRET_KEY').':'.strtotime('+5 minutes'));

        $response = $this->post('/api/v1.0/reset_password', [
            'user' => $reset_token,
            'password' => '!1dk[hmdmdHsj1',
            'password_confirmation' => '!1dk[hmdmdHsj1',
        ]);

        $response->assertOk();
    }

    /** @test */
    public function reset_password_expiration_work_correctly()
    {

        $user = User::factory()->create();

        $reset_token = encrypt($user->email.':'.env('PASSWORD_RESET_SECRET_KEY').':'.Carbon::now()->addMinutes(5)->timestamp);

        $this->travel(6)->minutes();

        $response = $this->post('/api/v1.0/reset_password', [
            'user' => $reset_token,
            'password' => '!1dk[hmdmdHsj1',
            'password_confirmation' => '!1dk[hmdmdHsj1',
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function reset_password_with_invalid_token_not_work()
    {
        $user = User::factory()->create();

        $reset_token = Str::random(256);

        $response = $this->post('/api/v1.0/reset_password', [
            'user' => $reset_token,
            'password' => '!1dk[hmdmdHsj1',
            'password_confirmation' => '!1dk[hmdmdHsj1',
        ]);

        $response->assertStatus(400);
    }

    // Omitted some tests due to lack of time
}
