<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_be_registered()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertOk();
        $this->assertCount(1, User::all());
    }

    /** @test */
    public function name_is_required()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => '',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function name_max_size_has_to_be_120_character()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => Str::random(125),
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function email_is_required()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => '',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function email_has_to_be_unique()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertOk();
        $this->assertCount(1, User::all());

        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
        $this->assertCount(1, User::all());
    }

    /** @test */
    public function email_is_valid_address()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbrgmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_is_required()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '',
            'password_confirmation' => '!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_has_valid_format()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_and_password_confirmation_min_size_has_to_be_6_character()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => Str::random(125),
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1Dk[',
            'password_confirmation' => '!1Dk['
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_and_password_confirmation_max_size_has_to_be_20_character()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => Str::random(125),
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj!1dk[hmdmdHsj'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_confirmation_is_required()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => ''
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_and_password_confirmation_has_to_be_same()
    {
        $response = $this->post('/api/v1.0/register', [
            'name' => 'jacob pourrahman',
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'password_confirmation' => '!1dk[hmdmdHsj435'
        ]);

        $response->assertStatus(400);
    }

    // Omitted some tests due to lack of time
}
