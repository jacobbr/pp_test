<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\Validator;
use Tests\TestCase;


class AuthenticateTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app['validator']->extend('captcha_api', function ($attribute, $value, $parameters) {
            return true;
        });
    }

    /** @test */
    public function registered_user_can_be_authenticate()
    {
        $user = User::factory()->create();

        $response = $this->post('/api/v1.0/authenticate', [
            'email' => $user->email,
            'password' => '!1dk[hmdmdHsj',
            'captcha' => '15',
            'captcha_key' => 'eyJpdiI6IjJHZktwc3pzOUhLNkZjWmIz'
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function unregistered_user_can_not_be_authenticate()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => 'terqr',
            'captcha_key' => '$2y$10$2ibHlkXavh5JtBWYTWr6i.owUleUPBSLEBNMuTgsBbznknIewGDXq',
        ]);

        $response->assertStatus(400);
    }


    /** @test */
    public function email_is_required()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => '',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => 'terqr',
            'captcha_key' => '$2y$10$2ibHlkXavh5JtBWYTWr6i.owUleUPBSLEBNMuTgsBbznknIewGDXq'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function email_is_valid_address()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbrgmail.com',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => 'terqr',
            'captcha_key' => '$2y$10$2ibHlkXavh5JtBWYTWr6i.owUleUPBSLEBNMuTgsBbznknIewGDXq'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_is_required()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '',
            'captcha' => 'terqr',
            'captcha_key' => '$2y$10$2ibHlkXavh5JtBWYTWr6i.owUleUPBSLEBNMuTgsBbznknIewGDXq'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function captcha_is_required()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => '',
            'captcha_key' => '$2y$10$2ibHlkXavh5JtBWYTWr6i.owUleUPBSLEBNMuTgsBbznknIewGDXq'
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function captcha_key_is_required()
    {
        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => 'terqr',
            'captcha_key' => ''
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function pair_captcha_and_captcha_key_has_to_be_valid()
    {
        $this->app['validator']->extend('captcha_api', function ($attribute, $value, $parameters) {
            return captcha_api_check($value, $parameters[0], $parameters[1] ?? 'default');
        });

        $response = $this->post('/api/v1.0/authenticate', [
            'email' => 'pourrahmanbr@gmail.com',
            'password' => '!1dk[hmdmdHsj',
            'captcha' => 'invalid_captcha',
            'captcha_key' => 'invalid_captcha_key'
        ]);

        $response->assertStatus(400);
    }

    // Omitted some tests due to lack of time
}
