<?php

namespace Tests\Feature\File;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;


class FileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_see_list_of_files()
    {

        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'bearer' . JWTAuth::fromUser($user)
        ])->get('api/v1.0/file/file');

        $response->assertStatus(200);
    }


    /** @test */
    public function unauthenticated_user_cant_see_list_of_files()
    {
        $response = $this->get('api/v1.0/file/file');

        $response->assertStatus(401);
    }

    /** @test */
    public function authenticated_user_can_get_list_of_directories()
    {

        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'bearer' . JWTAuth::fromUser($user)
        ])->get('api/v1.0/file/directory');

        $response->assertStatus(200);
    }

    /** @test */
    public function unauthenticated_user_cant_see_list_of_directories()
    {
        $response = $this->get('api/v1.0/file/directory');

        $response->assertStatus(401);
    }

    /** @test */
    public function authenticated_user_can_create_directory()
    {
        Storage::fake('pp_test');

        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'bearer' . JWTAuth::fromUser($user)
        ])->post('api/v1.0/file/directory', [
            'name' => 'valid_directory_name'
        ]);

        Storage::disk('pp_test')->assertExists('valid_directory_name');
        $response->assertStatus(200);
    }

    /** @test */
    public function unauthenticated_user_cant_create_directory()
    {
        $response = $this->post('api/v1.0/file/directory');

        $response->assertStatus(401);
    }

    /** @test */
    public function authenticated_user_can_create_file()
    {

        Storage::fake('pp_test');

        $user = User::factory()->create();

        $response = $this->withHeaders([
            'Authorization' => 'bearer' . JWTAuth::fromUser($user)
        ])->post('api/v1.0/file/file', [
            'name' => 'valid_file_name'
        ]);

        Storage::disk('pp_test')->assertExists('valid_file_name');
        $response->assertStatus(200);
    }

    /** @test */
    public function unauthenticated_user_cant_create_file()
    {
        $response = $this->get('api/v1.0/file/file');

        $response->assertStatus(401);
    }

    // Omitted some tests due to lack of time
}
