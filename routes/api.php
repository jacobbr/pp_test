<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate','AuthenticateController@authenticate');
Route::post('refresh_token','AuthenticateController@refreshToken');
Route::post('logout','AuthenticateController@logout');
Route::get('captcha','AuthenticateController@captcha');
Route::post('register','AuthenticateController@register');
Route::post('change_password','AuthenticateController@passwordChange');
Route::post('reset_password/mail','AuthenticateController@resetPasswordMail');
Route::post('reset_password','AuthenticateController@resetPassword');

Route::middleware(['jwt.auth'])->group(function () {

    Route::resource('process', 'ProcessController');

    Route::post('file/directory', 'FileController@createDirectory')->middleware('CommandInjectionProtection');
    Route::get('file/directory', 'FileController@getAllDirectories');
    Route::post('file/file', 'FileController@createFile')->middleware('CommandInjectionProtection');
    Route::get('file/file', 'FileController@getAllFiles');

});
